#ifndef __COLLECTOR_H__
#define __COLLECTOR_H__

#include <string>
#include <data/types.h>
// Make importing macos.h in main.cpp unnecessary
#include <data/macos.h>

namespace collector {

/**
 * Returns the OS's name.
 * 
 * If this were Python or Java, such a funtion wouldn't be needed.
 */
std::string os_name(OS os);

/**
 * Detects and returns the operating system. 
 * 
 * Taken from https://stackoverflow.com/a/5920028/12057978
 */
const OS detect_os();

/**
 * Returns a model of the current cpu load state.
 * 
 * @returns A cpu struct.
 */
cpu get_cpu_load();

/**
 * Returns a model of the current memory load state.
 * 
 * @returns A mem struct. 
 */
mem get_memory_load();


std::string generate_cpu_repr(cpu);
std::string generate_mem_repr(mem);

} // namespace collector

#endif