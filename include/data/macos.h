#ifndef __MACOS_H__
#define __MACOS_H__

#include <data/collector.h>
#include <data/types.h>

namespace collector {

/**
 * Returns the memory usage from MacOS.
 * 
 * Heavily inspired by https://stackoverflow.com/a/1911863/12057978
 */
mem get_mem_load_from_macos();


} //namespace collector

#endif //__MACOS_H__