#ifndef __TYPES_H__
#define __TYPES_H__

#include <string>

namespace collector {

enum OS {
    unsupported = 0,
    win = 1, 
    mac = 2, 
    linux = 3, 
};

struct cpu {
    double current;
    double max;
    std::string repr;
};

struct mem  {
    double used;
    double total;
    double free;
    double cache;
    std::string repr;
};

} // namespace collector

#endif