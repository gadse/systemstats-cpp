#include "data/collector.h"
#include <iostream>

using namespace std;

ostream& operator << (ostream &os, collector::cpu cpu) {
    return (os << cpu.repr);
}

ostream& operator << (ostream &os, collector::mem mem) {
    return (os << mem.repr);
}

int main() {
    cout << "Mock CPU: " << collector::get_cpu_load() << endl;
    cout << "Mock Memory: " << collector::get_memory_load() << endl;
    cout << "----------" << endl;
    cout << "Detected OS: " << collector::os_name(collector::detect_os()) << endl;
    cout << "OS Memory: " << collector::get_mem_load_from_macos() << endl;
    return 0;
}

