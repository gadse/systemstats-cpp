#include "data/collector.h"
#include <string>   
#include <iostream>
#include <sstream>
#include <iomanip>

namespace collector {

std::string os_name(OS os) {
    if (os == OS::win) {
        return "Windows";
    } else if (os == OS::mac) {
        return "MacOS";
    } else if (os == OS::linux) {
        return "Linux";
    } else {
        return "unsupported";
    }
};

const OS detect_os() {
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
    //define something for Windows (32-bit and 64-bit, this part is common)
    #ifdef _WIN64
        return OS::windows;
    #else
        return OS::windows;
    #endif
    #elif __APPLE__
        #include <TargetConditionals.h>
        #if TARGET_IPHONE_SIMULATOR
            return OS:unsupported;
        #elif TARGET_OS_IPHONE
            return OS:unsupported;
        #elif TARGET_OS_MAC
            return OS::mac;
        #else
            throw runtime_error("Unknown Apple platform");
        #endif
    #elif __linux__
        return OS::linux;
    #elif __unix__ // all unices not caught above
        return OS:unsupported;
    #elif defined(_POSIX_VERSION)
        return OS:unsupported;
    #else
        throw runtime_error("Completely unknown platform");
    #endif
}



cpu get_cpu_load() {
    cpu result = cpu();
    result.max = 100.0;
    result.current = 42.0;
    result.repr = generate_cpu_repr(result);
    return result;
}

mem get_memory_load() {
    auto result = mem();
    result.total = 1337;
    result.used = 42;
    result.free = result.total - result.used;
    result.repr = generate_mem_repr(result);
    return result;
}

std::string generate_cpu_repr(cpu cpu) {
    std::stringstream stream = std::stringstream();
    // Configure stream to fixed floats with 2 decimals precision
    stream << std::fixed << std::setprecision(2);
    stream << cpu.current << " / " << cpu.max;
    return stream.str();
}

std::string generate_mem_repr(mem memory) {
    std::stringstream stream = std::stringstream();
    // Configure stream to fixed floats with 2 decimals precision
    stream << std::fixed << std::setprecision(2);
    stream << memory.used << " GB"
        << " / " << memory.total << " GB"
        << " (" << memory.free << " GB free"
        << " with " << memory.cache << " GB cached)";
    return stream.str();
}

} // Namespace collector