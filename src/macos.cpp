/**
 * Implements MacOS-specific access to system statistice.
 * 
 * Mostly adapted from https://stackoverflow.com/a/1911863/12057978
 */
#include <data/collector.h>

#include <sys/types.h>
#include <sys/sysctl.h>
#include <cmath>
#include <vector>

#include <mach/vm_statistics.h>
#include <mach/mach_types.h>
#include <mach/mach_init.h>
#include <mach/mach_host.h>

namespace collector {

double BYTES_IN_GIBIBTES = std::pow(2, 30) * 1.0;

double get_max_mem() {
    int mib[2];
    int64_t physical_memory_in_bytes;
    mib[0] = CTL_HW;
    mib[1] = HW_MEMSIZE;
    auto length = sizeof(int64_t);
    int max_mem = sysctl(mib, 2, &physical_memory_in_bytes, &length, NULL, 0);
    return physical_memory_in_bytes / BYTES_IN_GIBIBTES;
}

double get_used_mem() {
    vm_size_t page_size;
    mach_port_t mach_port;
    mach_msg_type_number_t count;
    vm_statistics64_data_t vm_stats;

    mach_port = mach_host_self();
    count = sizeof(vm_stats) / sizeof(natural_t);
    if (KERN_SUCCESS == host_page_size(mach_port, &page_size) &&
        KERN_SUCCESS == host_statistics64(mach_port, HOST_VM_INFO,
                                        (host_info64_t)&vm_stats, &count)) {
       /* Calculating used and free memory this way roughly matches the number
        * given by the activity monitor:
        * used_memry riughly equals the repurted numbers for used minus cached. 
       */
        long long used_memory = ((int64_t)vm_stats.active_count +
            (int64_t)vm_stats.inactive_count +
            (int64_t)vm_stats.internal_page_count +
            (int64_t)vm_stats.wire_count) *  
            (int64_t)page_size;
        return used_memory / BYTES_IN_GIBIBTES;
    } else {
        return std::nan("");
    }
}

mem get_mem_load_from_macos() {
    mem result = mem();
    result.total = get_max_mem();
    result.used = get_used_mem();
    result.free = result.total - result.free;
    result.repr = generate_mem_repr(result);
    return result;
}


}