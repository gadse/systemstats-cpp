===========
systemstats
===========

This little toy project aims at delivering CPU and memory usage 
measurement at very low cost. No heavyweight cross platform library, 
just OS-specific operations.


Usage
------

Navigate to a directory on your system and::

  # checkout
  git clone https://gitlab.com/gadse/systemstats-cpp.git
  # compile
  cd systemstats-cpp/build
  cmake ..
  make
  # make target executable
  chmod +x systemstats_binary
  # execute
  ./systemstats_binary


Why bother?
-----------

You might find yourself asking questions like this one:

    Aren't there enough tools to measure or even log that?

Yes there are, glad you asked! I'm writing this because I'm just tired
of "Taskmanager" and similar applications using up 10-20% of my CPU 
at times. I want to measure (and log) these numbers without impacting
other running applications.


Why C++?
--------

For several reasons:
  - I want at least okay-ish cross-compile support, unlike `Rust`_.
  - I want to brush up on my C++ skills
  - I wanted to take a language I haven't used for a while
  - I had quite some fun with C++ during my university days

.. _Rust: https://gitlab.com/gadse/systemstats/-/pipelines/122136818


